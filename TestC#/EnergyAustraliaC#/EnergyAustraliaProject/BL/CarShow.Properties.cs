﻿using System.Collections.Generic;

namespace EnergyAustralia
{
    /// <summary>
    /// This is the model class for the carshow api.All properties of the json is mapped in this class
    /// </summary>
    public partial class CarShow
    {
        public string Name { get; set; }
        public List<Cars> Cars { get; set; }
    }

    public class Cars
    {
        public string Make { get; set; }
        public string Model { get; set; }
    }
}
