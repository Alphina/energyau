﻿using System.Collections.Generic;

namespace EnergyAustralia
{
    /// <summary>
    /// This class contains method calls corresponding to carshow api in DL class
    /// </summary>
    public partial class CarShow
    {
        /// <summary>
        /// This method returns the list of carshow returned by the API
        /// </summary>
        /// <returns></returns>
        public static List<CarShow> GetCarShowDetails()
        {
            CarShowManager carShowManager = new CarShowManager();
            return carShowManager.GetCarShowDetails();
        }
    }
}
