﻿

using Newtonsoft.Json;

using System;
using System.IO;
using System.Net;
namespace EnergyAustralia
{
    /// <summary>
    /// This is the helper class for HttpWebRequest and JsonDeserialization using newtonsoft
    /// </summary>
    public class RestfulWebClient
    {
        public string BaseUrl { get; set; }

        private static RestfulWebClient m_RestfulWebClientObject;
        public static RestfulWebClient RestfulWebClientObject
        {
            get
            {
                if (m_RestfulWebClientObject == null)
                {
                    m_RestfulWebClientObject = new RestfulWebClient();

                }
                return m_RestfulWebClientObject;
            }
            set
            {
                m_RestfulWebClientObject = value;
            }
        }

        /// <summary>
        /// This method helps in creating HttpWebRequest
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        private HttpWebRequest CreateRequest(string url, string method)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url.Contains("://") ? url : BaseUrl + url);

            request.Method = method;
            request.Accept = "application/json";
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.ContentLength = 0;
            return request;
        }

        /// <summary>
        /// This method handles the Getrequest for api call
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        public T Get<T>(string url)
        {
            HttpWebRequest request = CreateRequest(url, WebRequestMethods.Http.Get);

            try
            {

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {


                    if (WasSuccessful(response))
                    {

                        return Deserialize<T>(response);
                    }
                    else
                    {

                        throw new Exception(Convert.ToString(response.StatusCode));
                    }
                }
            }
            catch (WebException ex)
            {
                HttpWebResponse response = ex.Response as HttpWebResponse;
                throw new Exception(Convert.ToString(response.StatusCode));
            }
        }
        /// <summary>
        /// This  method checks whether status of HttpWebResponse
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public bool WasSuccessful(HttpWebResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.NoContent)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// This method helps in deserialisation for Json using newtonsoft
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="response"></param>
        /// <returns></returns>
        public T Deserialize<T>(HttpWebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                using (StreamReader sr = new StreamReader(responseStream))
                {
                    var reader = sr.ReadToEnd();

                    return JsonConvert.DeserializeObject<T>(reader);

                }
            }
        }
    }
}

