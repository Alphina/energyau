﻿using System.Collections.Generic;

namespace EnergyAustralia
{
 /// <summary>
/// This class handles the api call for carshow
/// </summary>
    public class CarShowManager
    {
        private static RestfulWebClient m_CarShowRestfulWebClient = RestfulWebClient.RestfulWebClientObject;
      
        private static RestfulWebClient CarShowRestfulWebClient
        {
            get
            {
                
                    if (m_CarShowRestfulWebClient.BaseUrl == null)
                    {
                        m_CarShowRestfulWebClient.BaseUrl = "http://eacodingtest.digital.energyaustralia.com.au/api/v1/";
                    }
                
                return m_CarShowRestfulWebClient;
            }
        }
        /// <summary>
        /// This method makes carshow api call
        /// </summary>
        /// <returns></returns>
        public List<CarShow> GetCarShowDetails()
        {
            return CarShowRestfulWebClient.Get<List<CarShow>>("cars");
        }
    }
}
