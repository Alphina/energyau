﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EnergyAustralia
{
    /// <summary>
    /// This is the mail loaded class
    /// </summary>
    class StarterClass
    {
        static void Main(string[] args)
        {
            try
            {
                var result = CarShow.GetCarShowDetails();
                //below query will get all cars sorted in alphabetical order of make name
                List<Cars> cars = result.SelectMany(x => x.Cars).GroupBy(x => new { x.Make, x.Model }).Select(x=>x.FirstOrDefault()).OrderBy(x=>x.Make).ToList();

                foreach (var car in cars)
                {
                    Console.WriteLine(car.Make);
                    Console.WriteLine(" "+car.Model);
                    
                    //This will get the list of all carshows attended by specific car
                    List<CarShow> carShows = result.Where(x => x.Cars.Where(y => y.Make == car.Make && y.Model == car.Model) != null).OrderBy(x=>x.Name).ToList();

                    foreach(var carShow in carShows)
                    {
                        
                        Console.WriteLine(carShow.Name);
                    }
                    Console.WriteLine();
                   
                }
                Console.ReadLine();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
          
        }
    }
}
