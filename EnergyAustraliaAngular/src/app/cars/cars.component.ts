import { Component, OnInit } from '@angular/core';
import { CarService } from '../Services/car.service';
import { CarShow } from '../Model/CarShow';
import { Cars } from '../Model/Cars';
import{CarDisplay} from '../Model/CarDisplay';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit {

  
  carShow: CarShow[]=[];
  cars:Cars[]=[];
  sortedCars:Cars[]=[];
  carDisplay:CarDisplay[]=[];

  constructor(private carService: CarService) { }
   ngOnInit() {
     this.GetShowDetails(); 
   }
   
   GetShowDetails()
   {
     try{
     
     this.carService.RestItemsServiceGetRestItems().subscribe(data=>
     {
      
       this.carShow=data;
       this.GetSortedDataToDisplay();
       
     })
     
   }
   catch(ex)
   {
    console.log(ex);
   }
  }
  //This method will select sorted distinct cars and make model to display
  GetSortedDataToDisplay()
  {
if(this.carShow!=null&&this.carShow.length>0)
{

 this.carShow.forEach(element => {
  element.cars.forEach(car=>
  {
  
   this.cars.push(car);
   
  });
   
 });
//This will take distinct cars with make and model
 Array.from(new Set(this.cars.map((cars: any) => cars.make&&cars.Model)))
 this.sortedCars=this.cars.slice(0); 
 this.sortedCars=this.cars.sort(this.compare);

 if(this.sortedCars!=null&&this.sortedCars.length>0)
 {
  this.sortedCars.forEach(element=>
    {
     
      let carDisplayModel=new CarDisplay();
      carDisplayModel.car=element;
      carDisplayModel.carShow=[];
      this.carShow.forEach(carShow => {
        
        carShow.cars.forEach(car=>
        {
        if(car.make==element.make&&car.model==element.model)
        {
          carDisplayModel.carShow.push(carShow.name);
         
        }
      
         
        });

       
         
       });
       this.carDisplay.push(carDisplayModel);
    });

   
 }


}

}

//This method will sort car in alphabetical order of car make
   compare(a,b) {
    if (a.make < b.make)
      return -1;
    if (a.make > b.make)
      return 1;
    return 0;
  }

}
