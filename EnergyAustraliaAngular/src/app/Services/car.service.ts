
import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { CarShow } from '../Model/CarShow';


@Injectable()
export class CarService {

  apiURL = 'http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars';  // URL to rest API



  constructor(private httpService: HttpClient) {

  }
  /**
 * This method makes api calls to get car show details
 */
  RestItemsServiceGetRestItems() {

    return this.httpService
      .get<CarShow[]>(this.apiURL);

  }



  private GenerateHeaders(): any {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json'
      })
    };


    return httpOptions;
  }


}
